package com.telstra.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import com.telstra.controller.App;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

//TODO Add mocking functionality for Controller part.
//TODO Move all configuration data to Appconfig  properties file
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
public class AppTest
{

	private static final Logger log = LoggerFactory.getLogger(App.class);
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setup() throws Exception
	{
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void retrieveDiscountsWithNoFilters() throws Exception
	{
		// TODO Add More Assertions for Unit test cases.
		MvcResult response = mockMvc.perform(get("/rest/v1/users/qa-test-user/discounts?productId="))

				.andExpect(status().isOk())
				.andReturn();
		log.info(response.getResponse().getContentAsString());
	}

	@Test
	public void discountsMatchingProductId() throws Exception
	{
		// TODO Add More Assertions for Unit test cases.
		MvcResult response = mockMvc.perform(get("/rest/v1/users/qa-test-user/discounts?productId=sku-1234567890"))
				.andExpect(status().isOk()).andReturn();

		log.info(response.getResponse().getContentAsString());
	}

	@Test
	public void userNotFound() throws Exception
	{   // TODO Add More Assertions for Unit test cases.
		MvcResult response = mockMvc.perform(get("/rest/v1/users/bad-user/discounts?productId=sku-1234567890"))
				.andExpect(status().isOk()).andReturn();
		log.info(response.getResponse().getContentAsString());
	}
}

package com.telstra.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerResource {

	private String uuid;
	
	private String name;
	
	private String address;
	
	private List<Discount> eligibleDiscounts;
	
	private List<PurchasedProduct> products;

	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the eligibleDiscounts
	 */
	public List<Discount> getEligibleDiscounts() {
		return eligibleDiscounts;
	}

	/**
	 * @param eligibleDiscounts the eligibleDiscounts to set
	 */
	public void setEligibleDiscounts(List<Discount> eligibleDiscounts) {
		this.eligibleDiscounts = eligibleDiscounts;
	}

	/**
	 * @return the products
	 */
	public List<PurchasedProduct> getProducts() {
		return products;
	}

	/**
	 * @param products the products to set
	 */
	public void setProducts(List<PurchasedProduct> products) {
		this.products = products;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomerResource [uuid=" + uuid + ", name=" + name + ", address=" + address + ", eligibleDiscounts="
				+ eligibleDiscounts + ", products=" + products + "]";
	}
	
}

package com.telstra.pojo;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchasedProduct {

	String productId;
	
	String description;
	
	BigDecimal originalPrice;
	
	BigDecimal finalPrice;
	
	Discount discountInformation;

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the originalPrice
	 */
	public BigDecimal getOriginalPrice() {
		return originalPrice;
	}

	/**
	 * @param originalPrice the originalPrice to set
	 */
	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}

	/**
	 * @return the finalPrice
	 */
	public BigDecimal getFinalPrice() {
		return finalPrice;
	}

	/**
	 * @param finalPrice the finalPrice to set
	 */
	public void setFinalPrice(BigDecimal finalPrice) {
		this.finalPrice = finalPrice;
	}

	/**
	 * @return the discountInformation
	 */
	public Discount getDiscountInformation() {
		return discountInformation;
	}

	/**
	 * @param discountInformation the discountInformation to set
	 */
	public void setDiscountInformation(Discount discountInformation) {
		this.discountInformation = discountInformation;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PurchasedProduct [productId=" + productId + ", description=" + description + ", originalPrice="
				+ originalPrice + ", finalPrice=" + finalPrice + ", discountInformation=" + discountInformation + "]";
	}
	
	
}

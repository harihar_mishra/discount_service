
package com.telstra.controller;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.telstra.pojo.CustomerResource;
import com.telstra.pojo.Discount;
import com.telstra.pojo.ErrorResource;
@SpringBootApplication
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class App {

	private static final Logger log = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
// TODO Move all Controller logic in separate class
@RestController
class RetriveDiscountController {

	@RequestMapping("/rest/v1/users/{user}/discounts")
	@Produces(MediaType.APPLICATION_JSON)
	String getDiscount(@PathVariable String user, @QueryParam("productId") String productId) {
		//TODO Environment specific variables should be part of property file
		//TODO Consider moving proxy logic to a different function
		String localProxyhost = "indpunsbd4intpxy01.ad.infosys.com";
		int localProxyPort = 80;
		String backEndURL = "http://52.65.9.120:9999/rest/v1/customers/qa-test-user";
		
		String json = null;
		ErrorResource errorResourse = null;
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		SimpleClientHttpRequestFactory clientHttpReq = new SimpleClientHttpRequestFactory();
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(localProxyhost, localProxyPort));
		clientHttpReq.setProxy(proxy);
		RestTemplate restTemplate = new RestTemplate(clientHttpReq);
		CustomerResource customerResource = restTemplate
				.getForObject(backEndURL, CustomerResource.class);
		List<Discount> lstDiscount = customerResource.getEligibleDiscounts();
		//TODO In real scenario user should be fetched from database or user registry
		if (user.equalsIgnoreCase("qa-test-user")) {
			try {
				if (productId == null || productId.isEmpty()) {

					json = ow.writeValueAsString(lstDiscount);

				} else if (!productId.isEmpty()) {
					for (Discount disc : lstDiscount) {

						if (disc.getProductId() != null && disc.getProductId().equals(productId)) {
							json = ow.writeValueAsString(disc);
							break;
						}
					}
				}
			} catch (JsonProcessingException e) {
				//TODO  No errors returned/defined for exceptions
				e.printStackTrace();
			}
		}
		else{
			//TODO Add custom exception class for Error code “ER0002” handling
			errorResourse = new ErrorResource();
			errorResourse.setCode("ER0002");
			errorResourse.setMessage("Bad request - user not found in the system");
			try {
				json = ow.writeValueAsString(errorResourse);
			} catch (JsonProcessingException e) {
				//TODO No errors returned/defined for exceptions
				e.printStackTrace();
			}
		}
		return json;
	}
}